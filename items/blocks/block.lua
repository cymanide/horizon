
local MergeTool = require "cum.dev.mergetool"

local Block = Object:extend()

local sqrt = math.sqrt
local Item = require "items.item"

local icons = {}
local MAX_DISTANCE = 8*8

function Block:new(count, type, str)
    self.stackCount = count or 1
    self.type = type
    assert(icons[type], "no icon for tile "..type)
    self.icon = icons[type]
end

function Block:use(op, owner)
    local layerMid = world:getLayer("mid")
    local mx, my = camera:screenToWorld(love.mouse.getPosition())
    local distance = sqrt((owner.x-mx)^2 + (owner.y-my)^2)
    local inventory = player.inventory
    if distance < MAX_DISTANCE then
        local cellx, celly = layerMid:pointToCell(mx,my)
        local tile = layerMid:getCell(cellx, celly)
        if op == 1 then -- break
            if tile ~= 0 then
                layerMid:setCell(cellx, celly, 0,11)
                inventory:add(Item("block:"..tile))
            end
        elseif op == 2 then --place
            if tile == 0 and self.stackCount > 0 then
                layerMid:setCell(cellx, celly, self.type,11)
                self.stackCount = self.stackCount - 1
            end
        end
		MergeTool.updateAround(layerMid, cellx, celly)
    end
end

function Block:canStackWith(item)
    return item.name == self.name
end 

local function _loadIcons()
    local baseFolder = "items/blocks/"
    local index = 0
    while true do 
        index = index + 1
        local file = baseFolder .. index .. ".png"
        local info = love.filesystem.getInfo(file)
        if info then
            icons[index] = ImageArray(file)
        else 
            return
        end
    end
end 
_loadIcons()

return Block
