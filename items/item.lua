
local INDEX = {}
local CACHE = {}

local function newItem(str, count)
    local colon = str:find(":")
    local name, param, item
    if colon then
        name = str:sub(0,colon-1)
        param = str:sub(colon+1,-1)
        param = tonumber(param) or param
    else 
        name = str
    end 
    if CACHE[name] then
        item = CACHE[name](count, param, str)
    elseif INDEX[name] then
        local newClass = love.filesystem.load(INDEX[name])()
        CACHE[name] = newClass
        item = newClass(count, param, str)
        item.name = str
    else 
        dev.log:print("failed to load item: "..name)
        return false
    end
    item.name = str
    return item
end 

local function findItems(folder)
    local items = love.filesystem.getDirectoryItems(folder)
    for _,name in ipairs(items) do
        local path = folder .. "/" .. name
        local info = love.filesystem.getInfo(path)
        if info.type == "directory" then
            findItems(path)
        elseif info.type == "file" then 
            if name:sub(-4) == ".lua" then 
                INDEX[name:sub(0,-5)] = path
            end 
        end 
    end 
end 

findItems("items")

return newItem
