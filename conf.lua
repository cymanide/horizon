
function love.conf(t)
    t.identity = "net.cosmiccrew.horizon"
    t.version = "11.0"
    t.window.width = 1024
    t.window.height = 512
    t.window.title = "Horizon"
    t.window.vsync = 1
    t.window.resizable = true
end
