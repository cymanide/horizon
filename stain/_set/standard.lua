return {
    name = "standard",
    size = 8,
    spacing = 1,
	folder = "img/tile/standard",

    { img="dirt", tagall="solid" },
	{ img="woodplanks", tagall="solid"},
	{ img="stone", tagall="solid" },
	{ img="sand", tagall="solid" },
	{ img="clay", tagall="solid" },
	{ img="mud", tagall="solid" },
	{ img="woodlog", tagall="solid" },
	{ img="coalore", tagall="solid" },
	{ img="glass", tagall="solid" },
	{ img="dirtbrick", tagall="solid" },
	{ img="grassblock", tagall="solid" },
	{ img="copperore", tagall="solid" },
	{ img="copperbrick", tagall="solid" },
	{ img="goldore", tagall="solid" },
	{ img="goldbrick", tagall="solid" },
	{ img="haybale", tagall="solid" },
	{ img="stonebrick", tagall="solid" },
	{ img="spruceplank", tagall="solid" },
	{ img="sprucelog", tagall="solid" },
	{ img="slate", tagall="solid" },
}
