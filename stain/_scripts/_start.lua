
local MergeTool = require "cum.dev.mergetool"

function cum.onWorldLoad()
	local godLayer = world:getLayer("god")
	godLayer = godLayer or world:addLayer("entity", "god", 128)
	if #godLayer.items < 1 then
		entityCreate(godLayer, {name = "cy_god"})
	end

	for _,layer in ipairs(world.layers) do
		if layer.type == "tile" then
			MergeTool.processLayer(layer)
		end
	end
end 

world:loadSTAIN("test3")
