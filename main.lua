
require "cum"

cum.init()

game.versionName = "prealpha a1"

cum.input.register {
    up = "key.w",
    down = "key.s",
    left = "key.a",
    right = "key.d",
    jump = "key.space",
    use1 = "mouse.1",
    use2 = "mouse.2"
}

require "commands"
