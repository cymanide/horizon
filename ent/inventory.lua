
local Inventory = Entity:extend()
Inventory.name = "inventory"

local log10, floor = math.log10, math.floor

local IMAGE = ImageArray("img/ui/inventory.png")
local WIDTH = IMAGE.w
local HEIGHT = IMAGE.h 

local Item = require "items.item"

local mx, my

function Inventory:start()
    self.rect = Rect(0,0, WIDTH, HEIGHT, self)
    self.x = -WIDTH/2
    self.y = -HEIGHT/2

    self.boxTop = Rect(0,0, WIDTH, 20)

    local spots = {}
    for x=1,12 do
        spots[x] = {}
        for y=1,5 do
            spots[x][y] = false
        end 
    end
    spots[1][1] = Item("block:19", 99)
    spots[2][2] = Item("block:20", 99)
    spots[3][3] = Item("block:15", 99)
    spots[4][4] = Item("block:16", 99)
    spots[5][5] = Item("block:17", 99)
    spots[6][4] = Item("block:18", 99)
    self.spots = spots

    self.heldItem = false

    self.hotbar = {}
    for x=0,12 do
        self.hotbar[x] = false
    end 
    self.grabbed = false
    self.grabCoords = {true, true}
end 

function Inventory:add(newItem)
    local ex, ey
    for x=1,12 do
        for y=1,5 do
            local item = self.spots[x][y]
            if item then
                if item:canStackWith(newItem) then
                    item.stackCount = item.stackCount + newItem.stackCount
                    return true
                end 
            elseif not ex then
                ex, ey = x, y
            end 
        end 
    end
    -- no stack found, resort to first empty spot
    if ex then
        self.spots[ex][ey] = newItem
        return true
    end    
end 

function Inventory:consume(itemName, count, type)
    count = count or 1
    for x=1,12 do
        for y=1,5 do
            local item = self.spots[x][y]
            if item and item.name == itemName and item.stackCount >= count
                    and not (type and item.type ~= type) then
                item.stackCount = item.stackCount - count
                if item.stackCount < 1 then
                    self.spots[x][y] = false 
                end
                return true
            end
        end 
    end 
    return false
end 

function Inventory:update() 
    local cam = self.god.camera
    mx, my = love.mouse.getPosition()
    mx, my = cam:screenToWorld(mx, my, true)
    local rmx, rmy = mx-self.x, my-self.y -- relative to window
    if input.use1.pressed then
        if self.boxTop:collidepoint(rmx, rmy) then
            self.grabbed = true
            self.grabCoords = {self.x-mx, self.y-my}
        else
            local cx, cy = self:toCell(rmx, rmy)
            local item = self.spots[cx] and self.spots[cx][cy]
            if cx <= 12 and cx > 0 and 
                cy <= 5 and cy > 0 then
                if item and self.heldItem and item:canStackWith(self.heldItem) then
                    item.stackCount = item.stackCount + self.heldItem.stackCount
                    self.heldItem = nil
                else
                    self.spots[cx][cy] = self.heldItem
                    self.heldItem = item
                end
            end 
        end 
    end 
    if input.use2.pressed then
        local cx, cy = self:toCell(rmx, rmy)
        local item = self.spots[cx] and self.spots[cx][cy]
        if cx <= 12 and cx > 0 and
                cy <= 5 and cy > 0 then
            if item then
                item.stackCount = item.stackCount - 1
                if self.heldItem then 
                    if item:canStackWith(self.heldItem) then 
                        self.heldItem.stackCount = self.heldItem.stackCount + 1
                    end 
                else 
                    self.heldItem = Item(item.name, 1)
                end 
            end
        end
    end     
    if input.use1.released then
        self.grabbed = false
    end
    if not self.rect:collidepoint(mx,my) and self.heldItem then
        if input.use1.down then
            self.heldItem:use(1, player)
        elseif input.use2.down then
            self.heldItem:use(2, player)
        end
    end 
    if self.grabbed then
        self.x = self.grabCoords[1] + mx
        self.y = self.grabCoords[2] + my
    end
    self:cleanEmptyStacks()
end 

function Inventory:cleanEmptyStacks()
    local spots = self.spots
    for y=1,5 do
        for x=1,12 do 
            local item = spots[x][y]
            if item and item.stackCount < 1 then
                spots[x][y] = nil
            end 
        end 
    end
    if self.heldItem and self.heldItem.stackCount < 1 then
        self.heldItem = nil 
    end
end 

local function renderItem(item, dx, dy)
    item.icon:draw(1, dx, dy)
    local count = item.stackCount
    font:print(""..count, dx-3 - 4*(log10(count)-1), dy+3)
end 

function Inventory:draw()
    IMAGE:draw(1, self.x, self.y)

    local spots = self.spots
    for cx=1,12 do
        for cy=1,5 do
            local item = spots[cx][cy]
            if item then
                local dx, dy = self:fromCell(cx,cy)
                renderItem(item, self.x + dx, self.y + dy)
            end 
        end 
    end 

    if self.heldItem then
        renderItem(self.heldItem, mx, my)
    end
end 

function Inventory:fromCell(cx, cy)
    return cx*17 - 11, cy*17 + 75
end 

function Inventory:toCell(x, y)
    local cx = floor((x + 13)/17)
    local cy = floor((y - 75)/17)
    return cx, cy
end 

return Inventory 
