
local Player = Entity:extend()
Player.name = "player"

local SPEED = 100
local JUMP_HEIGHT = 8*5
local GRAVITY = 1200
local JUMP_VEL = math.sqrt(JUMP_HEIGHT*GRAVITY*2)

function Player:start()
    self.rect = Rect(-6,-14,12,28, self)
    self.bound = true
    self.ya = GRAVITY

    self.inventory = true

    camera.zoom = 3

    self.canJump = false
    player = self
end

function Player:update()
    if input.left.down then
        self.xv = -SPEED
    elseif input.right.down then
        self.xv = SPEED
    else
        self.xv = 0
    end
    if input.jump.pressed then
        if self.canJump then
            self.yv = -JUMP_VEL
        end
    end

    self.canJump = false
end

function Player:postUpdate()
    camera.x = self.x
    camera.y = self.y
end

function Player:onBoundHit(side)
    if side==2 then
        self.canJump = true
    end
end

function Player:draw()
    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", self.rect:get())
    love.graphics.setColor(1, 1, 1)
end

return Player
