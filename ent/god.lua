
local God = Entity:extend()
God.name = "cy_god"

local lgfx = love.graphics

function God:start()
    self.rect = Rect(0,0, 10,10, self)
    self.ui = LayerEntity("ui", 128)
    self.inventory = entityCreate(self.ui, {name="inventory"})
    self.inventory.god = self
    player.inventory = self.inventory
    self.camera = Camera()
    self.camera.zoom = 2
end 

function God:update()
    self.inventory:update()
    self.x = camera.x
    self.y = camera.y
end 

function God:draw()
    if dev.editor.active then return end
    local was_camera = camera
    camera = self.camera
    lgfx.push()
    self.camera:preDraw()
    self.ui:draw()
    lgfx.pop()
    camera = was_camera
end 

return God 
