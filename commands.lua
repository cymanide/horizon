
local Item = require "items.item"

local com = {
    ["give"] = function (name, count)
        player.inventory:add(Item(name, count))
    end 
}

implementValues(dev.commands, com)